import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class GetData {


    public byte[] getFileInBytes(File f) throws IOException {
        FileInputStream fis = new FileInputStream(f);
        byte[] fbytes = new byte[(int) f.length()];
        fis.read(fbytes);
        fis.close();
        return fbytes;
    }

    public String GetPublicKey() throws Exception{

        GetData gd = new GetData();
        return new String( gd.getFileInBytes(new File("KeyPair/ServerPublicKey")), Charset.defaultCharset());
    }
}
